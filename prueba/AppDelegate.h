//
//  AppDelegate.h
//  prueba
//
//  Created by Alejandro Arce on 26/9/16.
//  Copyright © 2016 Alejandro Arce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

