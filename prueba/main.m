//
//  main.m
//  prueba
//
//  Created by Alejandro Arce on 26/9/16.
//  Copyright © 2016 Alejandro Arce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
